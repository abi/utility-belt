# utility-belt

Useful scripts used during my work at the Wikimedia Foundation

## List
1. `export-import-translatable-bundle-cmd-builder.php` - Generates commands to export and import translatable bundles from one wiki to another.
