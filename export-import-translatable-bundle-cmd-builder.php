<?php
# Generates export / import commands for translatable bundles from
# one wiki to another.
# Sample input: https://phabricator.wikimedia.org/T354473
declare( strict_types = 1 );

$SOURCE_WIKI_URL = 'https://meta.wikimedia.org/wiki/';
$TARGET_WIKI_URL = 'https://foundation.wikimedia.org/wiki/';
$SEPARATOR = '->';
$SOURCE_WIKI = 'metawiki';
$TARGET_WIKI = 'foundationwiki';
$IMPORT_USER = 'GVarnum-WMF';
$INTERWIKI_PREFIX = 'metawiki';
$IMPORT_COMMENT = 'See phab:T354473';

if ( !isset( $argv[1] ) ) {
    die( 'Provide a path containing the pages to export and import' );
}

$filePath = $argv[1];
$fileContent = file_get_contents( $filePath );
if ( $fileContent === false ) {
    die( "Could not get file contents from $filePath" );
}

$lines = explode("\n", $fileContent );
$pageList = [];
foreach ( $lines as $line ) {
    if ( trim( $line ) === '' ) {
        continue;
    }

    $currentPageList = array_map('trim', explode( $SEPARATOR, $line ) );
    $sourcePageName = str_replace( $SOURCE_WIKI_URL, '', $currentPageList[0] );
    $targetPageName = str_replace( $TARGET_WIKI_URL, '', $currentPageList[1] );
    $tmpFileName = '/tmp/' . str_replace( '/', '_', $sourcePageName ) . ".xml";
    $pageList[] = [ $sourcePageName, $targetPageName, $tmpFileName, trim( $line ) ];
}

foreach ( $pageList as $i => [ $sourcePageName, $targetPageName, $tmpFileName, $line ] ) {
    $exportCommand = "mwscript extensions/Translate/scripts/exportTranslatableBundle.php --wiki=$SOURCE_WIKI " .
        "--translatable-bundle \"$sourcePageName\" --filename $tmpFileName --include-talk-pages --include-subpages";

    $importCommand = "mwscript extensions/Translate/scripts/importTranslatableBundle.php --wiki=$TARGET_WIKI " .
        "$tmpFileName  --user \"$IMPORT_USER\" --interwiki-prefix  $INTERWIKI_PREFIX --comment \"$IMPORT_COMMENT\" " .
        "--target-name \"$targetPageName\" --override";

    $line = "# $i. $line";
    echo $line . "\n";
    echo $exportCommand . "\n";
    echo $importCommand . "\n";
    echo "\n";
}