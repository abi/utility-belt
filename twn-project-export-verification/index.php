<?php
# Identifies projects missing from export
# Run /srv/mediawiki/workdir/maintenance/run /srv/mediawiki/workdir/extensions/Translate/scripts/expand-groupspec.php '*' --exportable
# on translatewiki.net server and update the export-group-spec.txt file in this folder
# Get repoconfig.yaml file from translatewiki repo and update the repconfig.yaml file in this folder

$repoConfigYaml = yaml_parse( trim( file_get_contents( __DIR__ . '/repoconfig.yaml') ) );
$groupSpecOutput = array_filter(
    explode(
        "\n",
        trim( file_get_contents( __DIR__ . '/export-group-spec.txt' ) )
    )
);

$missingGroups = [];
foreach ( $groupSpecOutput as $groupId ) {
    $matched = false;
    foreach( $repoConfigYaml as $yamlSpec ) {
        if ( !isset( $yamlSpec[ 'group'] ) ) {
            continue;
        }

        $matcher = explode( ',', $yamlSpec['group' ] );
        foreach( $matcher as $wildcard ) {
            echo "Matching $groupId with $wildcard\n";
            if ( fnmatch( $wildcard, $groupId ) ) {
                $matched = true;
                break;
            }
        }
    }

    if ( !$matched ) {
        $missingGroups[] = $groupId;
    }
}

echo "\n";
echo "Missing groups in repconfig.yaml: \n";
echo implode( "\n - ", $missingGroups );
echo "\n\n";